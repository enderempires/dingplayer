package com.enderempires.playerding.handler;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.Plugin;

public class EventListener implements Listener{

	public Plugin plugin;
	
	public EventListener(Plugin instance) {
		plugin = instance;
		//registers this eventlistener
		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	//gathers if the player gets damaged
	public void dmg(final EntityDamageEvent event) {
		Entity en = event.getEntity();
		
		//checks if this entity is the player
		if(en instanceof Player) {
			Player player = (Player)en;
			//plays the level up sound at the player's location.
			//not sure what the last two params are
			player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 0);
		}
		
	}
	
}
